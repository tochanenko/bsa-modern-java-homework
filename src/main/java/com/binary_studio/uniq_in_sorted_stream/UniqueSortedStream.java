package com.binary_studio.uniq_in_sorted_stream;

import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Stream;

public final class UniqueSortedStream {

	private UniqueSortedStream() {
	}

	public static <T> Stream<Row<T>> uniqueRowsSortedByPK(Stream<Row<T>> stream) {
		AtomicReference<Row<T>> prev = new AtomicReference<>(null);
		return stream.filter(item -> {
			if (prev.get() == null || !item.equals(prev.get())) {
				prev.set(item);
				return true;
			}
			return false;
		});
	}

}
