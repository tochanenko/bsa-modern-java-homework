package com.binary_studio.tree_max_depth;

import java.util.LinkedList;
import java.util.Queue;

public final class DepartmentMaxDepth {

	private DepartmentMaxDepth() {
	}

	public static Integer calculateMaxDepth(Department rootDepartment) {
		if (rootDepartment == null) {
			return 0;
		}

		Queue<Department> q = new LinkedList<>();

		// Test as offer() function
		q.add(rootDepartment);
		q.add(null);
		Integer height = 1;

		Department currentNode;
		while (!q.isEmpty()) {
			currentNode = q.remove();
			if (currentNode == null && !q.isEmpty()) {
				height++;
				q.add(null);
			}
			else if (currentNode != null) {
				for (Department department : currentNode.subDepartments) {
					if (department != null) {
						q.add(department);
					}
				}
			}
		}

		return height;
	}

}
