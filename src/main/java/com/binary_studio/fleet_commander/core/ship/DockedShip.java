package com.binary_studio.fleet_commander.core.ship;

import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.exceptions.InsufficientPowergridException;
import com.binary_studio.fleet_commander.core.exceptions.NotAllSubsystemsFitted;
import com.binary_studio.fleet_commander.core.ship.contract.ModularVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class DockedShip implements ModularVessel {

	private String name;

	private PositiveInteger shieldHP;

	private PositiveInteger hullHP;

	private PositiveInteger powergridOutput;

	private PositiveInteger capacitorAmount;

	private PositiveInteger capacitorRechargeRate;

	private PositiveInteger speed;

	private PositiveInteger size;

	private AttackSubsystem attackSubsystem = null;

	private DefenciveSubsystem defenciveSubsystem = null;

	public static DockedShip construct(String name, PositiveInteger shieldHP, PositiveInteger hullHP,
			PositiveInteger powergridOutput, PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate,
			PositiveInteger speed, PositiveInteger size) {
		if (name == null || name.isBlank() || name.isEmpty()) {
			throw new IllegalArgumentException("Name should be not null and not empty");
		}
		DockedShip ship = new DockedShip();
		ship.name = name;
		ship.shieldHP = shieldHP;
		ship.hullHP = hullHP;
		ship.powergridOutput = powergridOutput;
		ship.capacitorAmount = capacitorAmount;
		ship.capacitorRechargeRate = capacitorRechargeRate;
		ship.speed = speed;
		ship.size = size;
		return ship;
	}

	@Override
	public void fitAttackSubsystem(AttackSubsystem subsystem) throws InsufficientPowergridException {
		if (subsystem == null) {
			this.attackSubsystem = null;
			return;
		}
		int powerGridConsumption = this.defenciveSubsystem == null ? 0
				: this.defenciveSubsystem.getPowerGridConsumption().value();
		powerGridConsumption += subsystem.getPowerGridConsumption().value();
		if (powerGridConsumption > this.powergridOutput.value()) {
			throw new InsufficientPowergridException(this.powergridOutput.value() - powerGridConsumption);
		}
		this.attackSubsystem = subsystem;
	}

	@Override
	public void fitDefensiveSubsystem(DefenciveSubsystem subsystem) throws InsufficientPowergridException {
		if (subsystem == null) {
			this.defenciveSubsystem = null;
			return;
		}
		int powerGridConsumption = this.attackSubsystem == null ? 0
				: this.attackSubsystem.getPowerGridConsumption().value();
		powerGridConsumption += subsystem.getPowerGridConsumption().value();
		if (powerGridConsumption > this.powergridOutput.value()) {
			throw new InsufficientPowergridException(this.powergridOutput.value() - powerGridConsumption);
		}
		this.defenciveSubsystem = subsystem;
	}

	public CombatReadyShip undock() throws NotAllSubsystemsFitted {
		if (this.defenciveSubsystem == null && this.attackSubsystem == null) {
			throw NotAllSubsystemsFitted.bothMissing();
		}
		else if (this.defenciveSubsystem == null) {
			throw NotAllSubsystemsFitted.defenciveMissing();
		}
		else if (this.attackSubsystem == null) {
			throw NotAllSubsystemsFitted.attackMissing();
		}

		return CombatReadyShip.construct(this.name, this.shieldHP, this.hullHP, this.powergridOutput,
				this.capacitorAmount, this.capacitorRechargeRate, this.speed, this.size, this.attackSubsystem,
				this.defenciveSubsystem);
	}

}
