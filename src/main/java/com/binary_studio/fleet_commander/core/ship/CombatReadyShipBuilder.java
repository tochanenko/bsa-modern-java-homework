package com.binary_studio.fleet_commander.core.ship;

import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;
import jdk.jfr.Experimental;

@Experimental
public class CombatReadyShipBuilder {

	private String name;

	private PositiveInteger shieldHP;

	private PositiveInteger hullHP;

	private PositiveInteger powergridOutput;

	private PositiveInteger capacitorAmount;

	private PositiveInteger capacitorRechargeRate;

	private PositiveInteger speed;

	private PositiveInteger size;

	private AttackSubsystem attackSubsystem = null;

	private DefenciveSubsystem defenciveSubsystem = null;

	public CombatReadyShipBuilder named(String name) {
		var builder = new CombatReadyShipBuilder();

		builder.size = PositiveInteger.of(1);
		builder.speed = PositiveInteger.of(1);

		builder.name = name;
		return builder;
	}

	public CombatReadyShipBuilder shieldHP(Integer val) {
		this.shieldHP = PositiveInteger.of(val);
		return this;
	}

	public CombatReadyShipBuilder hullHP(Integer val) {
		this.hullHP = PositiveInteger.of(val);
		return this;
	}

	public CombatReadyShipBuilder pg(Integer val) {
		this.powergridOutput = PositiveInteger.of(val);
		return this;
	}

	public CombatReadyShipBuilder capacitorAmount(Integer val) {
		this.capacitorAmount = PositiveInteger.of(val);
		return this;
	}

	public CombatReadyShipBuilder capacitorRechargeRate(Integer val) {
		this.capacitorRechargeRate = PositiveInteger.of(val);
		return this;
	}

	public CombatReadyShipBuilder speed(Integer val) {
		this.speed = PositiveInteger.of(val);
		return this;
	}

	public CombatReadyShipBuilder size(Integer val) {
		this.size = PositiveInteger.of(val);
		return this;
	}

	public CombatReadyShipBuilder attackSubsystem(AttackSubsystem attackSubsystem) {
		this.attackSubsystem = attackSubsystem;
		return this;
	}

	public CombatReadyShipBuilder defenciveSubsystem(DefenciveSubsystem defenciveSubsystem) {
		this.defenciveSubsystem = defenciveSubsystem;
		return this;
	}

	public CombatReadyShip construct() {
		return CombatReadyShip.construct(this.name, this.shieldHP, this.hullHP, this.powergridOutput,
				this.capacitorAmount, this.capacitorRechargeRate, this.speed, this.size, this.attackSubsystem,
				this.defenciveSubsystem);
	}

}
