package com.binary_studio.fleet_commander.core.subsystems;

import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;

public final class AttackSubsystemImpl implements AttackSubsystem {

	private String name;

	private PositiveInteger baseDamage;

	private PositiveInteger optimalSize;

	private PositiveInteger optimalSpeed;

	private PositiveInteger capacitorConsumption;

	private PositiveInteger powergridRequirments;

	public static AttackSubsystemImpl construct(String name, PositiveInteger powergridRequirments,
			PositiveInteger capacitorConsumption, PositiveInteger optimalSpeed, PositiveInteger optimalSize,
			PositiveInteger baseDamage) throws IllegalArgumentException {
		if (name == null || name.isBlank() || name.isEmpty()) {
			throw new IllegalArgumentException("Name should be not null and not empty");
		}
		AttackSubsystemImpl system = new AttackSubsystemImpl();
		system.name = name;
		system.powergridRequirments = powergridRequirments;
		system.capacitorConsumption = capacitorConsumption;
		system.optimalSpeed = optimalSpeed;
		system.optimalSize = optimalSize;
		system.baseDamage = baseDamage;

		return system;
	}

	@Override
	public PositiveInteger getPowerGridConsumption() {
		return this.powergridRequirments;
	}

	@Override
	public PositiveInteger getCapacitorConsumption() {
		return this.capacitorConsumption;
	}

	@Override
	public PositiveInteger attack(Attackable target) {
		double sizeReductionModifier = target.getSize().value() >= this.optimalSize.value() ? 1
				: (double) target.getSize().value() / this.optimalSize.value();
		double speedReductionModifier = target.getCurrentSpeed().value() <= this.optimalSpeed.value() ? 1
				: (double) this.optimalSpeed.value() / (2 * target.getCurrentSpeed().value());
		return new PositiveInteger(
				(int) Math.ceil(this.baseDamage.value() * Math.min(sizeReductionModifier, speedReductionModifier)));
	}

	@Override
	public String getName() {
		return this.name;
	}

}
